import unittest

from lib.TvArchiveDirectory import TvArchiveDirectory

"""
" Test the Archive Directory class to see if it's methods are working correctly
"""
class TestArchiveDirectory(unittest.TestCase):
    
    """
    Test whether the script is pulling out the correct keyWords for different
    tv show names
    """
    def testKeyWords(self):

        # different show titles with the number of keywords they should have
        shows = {
            "The 100": 1,
            "Arrow": 1,
            "The Big Bang Theory": 3,
            "Breaking Bad": 2,
            "Better Call Saul": 3,
            "Dexter": 1,
            "the Flash": 1,
            "Game of Thrones": 2,
            "How I Met your Mother": 5,
            "The Office (US)": 2,
            "Orphan Black": 2,
            "The Last Man on Earth": 3,
            "Vikings":  1,
        }

        archivesDir = TvArchiveDirectory("downloads")

        """
        Run through all the above show names, and check if the keywords
        extracted match the expected number of keywords
        """
        for showName, numKeyWords in shows.iteritems():
            print("testKeyWords: " + showName)
            keyWords = archivesDir.getKeyWords(showName)
            self.assertEqual(len(keyWords), numKeyWords)

"""
Main methods
"""
def main():
    print("Running TestArchiveDirectory tests:")
    unittest.main()

if __name__ == "__main__":
    main()
