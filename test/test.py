import errno
import os
import shutil
import subprocess
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/lib")
import config

# test data
shows = {
    "Friends": [],
    "Arrow": [
        "arrow.xvid.lol[hdtv]s02e03.mp4",
    ],
    "the Last Man on Earth": [
        "The.Last.Man.On.Earth.s01e01.mkv",
    ],
    "How to Get Away with Murder": [],
    "the 100": [
        "xvid.the.100.s01e02.avi",
    ],
    "Empire": [
        "empire.s01e04.mp4",
    ]
}

def main():
    print "setting up..."
    setup()
    print "seeding..."
    seed()
    print "sorting..."
    sort()
    print "verifying..."
    verify()

def setup():
    cwd = os.path.dirname(os.path.realpath(__file__))

    # just to make sure we are not overriding the actual dest_dir
    try:
        assert(config.dest_dir.find("home") != -1)
        assert(config.dest_dir == cwd + "/archive")
    except:
        print("ERROR: check testing directories are set!!")
        print("src_dir = " + config.src_dir)
        print("dest_dir = " + config.dest_dir)
        sys.exit(1)

    print("Source directory: " + config.src_dir)
    print("Destin directory: " + config.dest_dir)

    mkdirP(config.dest_dir)
    mkdirP(config.src_dir)

    print("[OK]")

def seed():

    print("cleaning...")
    for show in shows:
        try:
            shutil.rmtree(os.path.join(config.dest_dir, show))
        except:
            pass
    print("[OK]")

    for show in shows:
        mkdirP(os.path.join(config.dest_dir, show))
        for episode in shows[show]:
            fname = os.path.join(config.src_dir, episode)
            touch(fname)
            assert(os.path.exists(fname))
    print("[OK]")

def sort():
    rootdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    subprocess.call([os.path.join(rootdir, "tv-cleanup.sh")])

def verify():
    for show in shows:
        eps = []
        for root, dirs, files in os.walk(os.path.join(config.dest_dir, show)):
            path = root.split('/')
            for file in files:
                eps.append(file)
        assert(len(eps) == len(shows[show]))
    print("[OK]")

# simple 'touch' implementation
def touch(fname):
    with open(fname, 'a'):
        os.utime(fname, None)

def mkdirP(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.exists(path):
            pass
        else: raise

if __name__ == "__main__":
    sys.exit(main())
