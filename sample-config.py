# tvdb API key. Used for downloading episode names
# can be obtained for free from http://thetvdb.com/?tab=apiregister
api_key = "<your-api-key>"

# the directory containing the tv-shows you'd like to clean up
src_dir = "~/Downloads"

# the archive directory where your tv-shows should be put
dest_dir = "/path/to/archive"

"""
" Application settings
" Settings for more fine-grained control over the application
"""

## the directory to use as a trash folder
#dir_trash = "~/tmp"

# delete the containing directory of a show if it contains no more video files
delete_empty_dirs = True

# whether or not to delete the sample files that some downloads provide
delete_samples = True
