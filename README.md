# TV Show Sorter

A Python script that will recursively clean a given directory by finding tv shows
and renaming them to their correct episode titles using
[thetvdb.com](http://www.thetvdb.com). 

## Installing

 * Edit `D_DOWNLOADS` and `D_ARCHIVE` in `tv-cleanup.sh`:
    * `D_DOWNLOADS`: the directory containing the files you want to clean
    * `D_ARCHIVE`: the directory with your tv shows ie. the place you want to
        put the new files

## Running

 * Example directory structures:
    
```
    D_ARCHIVE/
    |--Friends/
    |----Season 1/
    |------01 - Pilot.mp4
    |----Season 2/
    |------01 - The One With Ross' New Girlfriend.mp4
    |--Game of Thrones/
    |--the Simpsons/

    D_DOWNLOAD/
    |--the.simpsons.s01e01.LOL.mp4
    |--game.of.thrones/
    |----readme.txt
    |----game.of.thrones.s02e04.mkv
```

 * Make sure that the directory `D_ARCHIVE` has folders with the names of the
   shows you want to clean up.
   For example, if you have episodes of the Simpsons you want to clean up, make
   sure that you have a directory called `Simpsons` or `the Simpsons` in
   `D_ARCHIVE`
 * run `tv-cleanup.sh`