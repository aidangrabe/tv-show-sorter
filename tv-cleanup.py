#!/usr/bin/env python

import sys

import lib.dldir as dldir
import lib.msg as msg
import lib.renamer as renamer
import lib.settings as settings
import lib.util as util
from lib.TvArchiveDirectory import TvArchiveDirectory


def main(args):

    msg.DEBUG = True
    msg.debug("Starting...")

    # get the tv show names from the archive
    tv_shows = TvArchiveDirectory(settings.get_path_opt('dest_dir')).list_show_titles()

    video_files = dldir.get_files(['avi', 'mp4', 'mkv'])

    files_to_move = renamer.get_files_to_rename(video_files, tv_shows)

    remove_samples([fname for fname, _ in files_to_move.iteritems()])

    # msg.debug(files_to_move)
    if len(files_to_move) == 0:
        msg.warn("No files found in downloads directory")
    else:
        msg.debug("Found {} files to move".format(len(files_to_move)))

    for ftm in files_to_move:
        renamer.move(ftm, files_to_move[ftm])


def remove_samples(files):
    """
    Remove files that contain the word 'sample' from an array of file names
    Args:
        files: an array of file names
    """
    if not settings.get_opt('delete_samples', False):
        return

    # find all file names that contain 'sample'
    sample_files = filter(lambda fname: 'sample' in fname.lower(), files)

    if len(sample_files) > 0:
        for sampleFile in sample_files:
            util.move_to_trash(sampleFile)

if __name__ == "__main__":
    sys.exit(main(sys.argv))
