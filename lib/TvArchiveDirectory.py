#!/usr/bin/env python

import os
import os.path
import sys

import msg


class TvArchiveDirectory:
    
    def __init__(self, source_dir):
        self.sourceDir = source_dir

        self.smallWords = ['the', 'of', 'and', 'but', 'on']

    """
    Get the important keywords from a given string s
    """
    def get_keywords(self, s):
        keywords = []
        words = s.replace(',', '').split()
        for word in words:
            wordLower = word.lower()
            if wordLower not in self.smallWords:
                keywords += [wordLower]
        return keywords

    """
    Scan the source directory for directories. These directories will be used
    as the Tv Show Titles
    """
    def list_show_titles(self):
        if not os.path.isdir(self.sourceDir):
            msg.err("TV Show source directory ({}) does not exist".format(self.sourceDir))
            sys.exit(1)

        tv_show_titles = []
        # get the tv show titles in the source directory
        for (dirpath, tv_show_titles, filenames) in os.walk(self.sourceDir):
            break
        if len(tv_show_titles) == 0:
            msg.err("No Tv Shows Found in Source Directory.")

        tv_shows = {}
        for tvShow in tv_show_titles:
            tv_shows[tvShow] = self.get_keywords(tvShow)

        return tv_shows
