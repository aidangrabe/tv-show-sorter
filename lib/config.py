import os.path

# tvdb API key. Used for downloading episode names
api_key = "<API-KEY>"

# the directory containing the tv-shows you'd like to clean up
src_dir = "/path/to/downloads"

# the archive directory where your tv-shows should be put
dest_dir = "/path/to/archive"

"""
" Application settings
" Settings for more fine-grained control over the application
"""

# the home directory '~'
dir_home = os.path.expanduser("~")

# the directory to store this application's data
dir_data = dir_home + "/.tv-shows"

# the directory to store the show ids in
dir_show_ids = dir_data + "/show-ids"

# the directory to contain the episode lists
dir_show_episodes = dir_data + "/shows"


"""
" Testing settings
"""

# debug
debug = False

# tests should only run if this is set to True
testing = False

# the source directory when testing is True
test_src_dir = "/path/to/testing/downloads"

# the destination directory when testing is True
test_dest_dir = "/path/to/testing/archive"
