#!/usr/bin/env python

import os

import config


def get_opt(varname, default):
    """
    Get an optional setting from the config file
    Args:
        varname: the name of the setting to get
        default: the default value used if the setting is empty or not set
    Returns:
        mixed - the type of the setting from config.py
    """
    value = default
    try:
        value = getattr(config, varname)
        if value is "":
            return default
    except AttributeError, e:
        # legacy settings
        try:
            value = os.environ[varname]
        except KeyError, e:
            pass
    return value


def get_path_opt(varname, default=''):
    """
    Args:
        varname: setting name
        default: the default value to return if the setting does not exist
    Returns:
        a path as a string
    """
    return os.path.expanduser(get_opt(varname, default))


def get(varname):
    return get_opt(varname, "")
