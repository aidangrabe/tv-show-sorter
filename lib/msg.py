#!/usr/bin/env python

import config

def make_color(c):
    return "\033[0;{}m".format(c)

class COLOR:
    RED     = make_color(31)
    GREEN   = make_color(32)
    YELLOW  = make_color(33)
    BLUE    = make_color(34)
    PURPLE  = make_color(35)
    TEAL    = make_color(36)
    RESET   = "\033[0m"

def debug(txt):
    if config.verbose:
        print "".join((COLOR.TEAL, str(txt), COLOR.RESET))

def err(txt):
    print "{}Error: {}{}".format(COLOR.RED, COLOR.RESET, str(txt))

# TODO
#def log(txt):
#    f = open("tv-show-log.log", "a+")
#    f.write("{}:\n{}".format(txt))
#    f.close()
#    sys.exit(1)

def success(txt):
    print COLOR.GREEN + str(txt) + COLOR.RESET

def warn(txt):
    print "{}Warning: {}{}".format(COLOR.YELLOW, COLOR.RESET, str(txt))
