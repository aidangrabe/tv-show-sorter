#!/usr/bin/env python

import re
import tvdb_api

def createEpisodeFileName(episodeName, seasonNumber, episodeNumber):
    """
    Create a file name from an episode name
    Args:
        episodeName: the name of the episode as a string
        seasonNumber: the number of the season as a string or number
        episodeNumber: the episode number as a string or number
    Returns:
        A string in the form "<Episode number> - <Episode Name>"
    """
    return "{num:02d} - {title}".format(num=int(episodeNumber), title=episodeName)

def getEpisodeNumber(f):
    regs = [
        ur".*s([0-9]{1,2})e([0-9]{1,2}).*",
        ur".*season\s*([0-9]{1,2}).*episode\s*([0-9]{1,2}).*",
        ur".*([0-9]{1,2})x([0-9]{1,2}).*"
    ]
    for reg in regs:
        matches = re.findall(reg, f, re.IGNORECASE)
        if len(matches) == 1:
            # we found the numbers
            break
    try:
        matches = matches[0]
    except IndexError:
        matches = (-1, -1)
    return matches

def getEpisodeName(showTitle, seasonNumber, episodeNumber):
    """
    Lookup an episodes name from tvdb
    Args:
        showTitle: the show title to search for as a string
        seasonNumber: the number of the season as a string or number
        episodeNumber: the episode number as a string or number
    Returns:
        A formatted string of the episodes name and number
    """
    seasonNumber = int(seasonNumber)
    episodeNumber = int(episodeNumber)

    tvdb = tvdb_api.Tvdb()
    episode = tvdb[showTitle][seasonNumber][episodeNumber]
    episodeName = episode['episodename']
    return createEpisodeFileName(episodeName, seasonNumber, episodeNumber)
