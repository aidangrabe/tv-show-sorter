import os
import os.path

from lib import settings
import msg


def get_files(extensions):
    msg.debug("Searching the downloads folder")
    dlfiles = []
    for (dirpath, dirnames, filenames) in os.walk(settings.get_path_opt('src_dir')):
        for f in filenames:
            if os.path.splitext(f)[1][1:].lower() in extensions:
                dlfiles += [dirpath + os.path.sep + f]
    return dlfiles
