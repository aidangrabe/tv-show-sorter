import errno
import os
import shutil
import tempfile

import settings


def mkdirp(path):
    """
    make a directory and parent directories if they don't exist
    similar to the bash command "mkdir -p"
    should be pronouced McDerp
    Args:
        path: the path to create as a string
    """
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.exists(path):
            pass
        else:
            raise


def find(fdir):
    """
    Similar to the unix `find` command
    Recursively lists all files in the given directory
    Args:
        fdir: string, the directory to start searching from
    Returns:
        An array of paths
    """
    files = []
    for (dirpath, dirnames, filenames) in os.walk(fdir):
        for filename in filenames:
            files.append(os.path.join(dirpath, filename))
    return files


def find_by_extension(fdir, ext):
    """
    Filter find(fdir) to only show paths that have the given extension(s)
    Args:
        fdir: string, the directory to search in
        ext: string|[string]|(string), an extension or list/tuple of extensions
    Returns:
        [string] - the file paths that have the given extension(s)
    """
    if not isinstance(ext, list):
        ext = [ext]
    ext = tuple(ext)
    return filter(lambda f: f.endswith(ext), find(fdir))


def move_to_trash(fpath):
    """
    Move a file or directory to the trash directory specified by the config.py
    Args:
        fpath: string, the path to move to the trash
    """
    trash_dir = settings.get_path_opt('dir_trash', tempfile.gettempdir())
    shutil.move(fpath, trash_dir)
