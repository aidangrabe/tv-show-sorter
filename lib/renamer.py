#!/usr/bin/env python

import os
import os.path
import shutil

import msg
import settings
import tvdb
import util


def get_files_to_rename(video_files, tv_shows):
    msg.debug("Searching for Tv Show keywords")
    files_to_move = {}

    # loop through the files we found in the downloads folder
    for video_file in video_files:
        possibilities = []          # files who matched all keywords

        # loop through the shows keywords and match against this file
        for tvShow in tv_shows:
            keywords_found = 0
            for keyword in tv_shows[tvShow]:
                if video_file.lower().find(keyword) != -1:
                    keywords_found += 1

            # if all this shows keywords were found, then it's a possibility
            if keywords_found == len(tv_shows[tvShow]):
                possibilities.append(tvShow)

        # find the most suitable tv show name
        most_suitable = ""
        num_keywords = 0

        # loop through shows who matched all their keywords, and pick the show
        # that matched the most
        for tvShow in possibilities:
            if most_suitable == "":
                most_suitable = tvShow
                num_keywords = len(tv_shows[tvShow])
            elif len(tv_shows[tvShow]) > num_keywords:
                most_suitable = tvShow
                num_keywords = len(tv_shows[tvShow])

        # select the most suitable tv show
        files_to_move[video_file] = most_suitable

    return files_to_move


def move(files_to_move, dest_dir):
    season, episode = tvdb.getEpisodeNumber(files_to_move)
    if season == -1 or episode == -1:
        msg.warn("Unable to extract season + episode number from file " + files_to_move)
        return

    try:
        new_name = tvdb.getEpisodeName(dest_dir, season, episode)
    except Exception as e:
        msg.warn("Failed to find name for season {}, episode {} of {}".format(season, episode, dest_dir))
        return

    if new_name == "":
        return
    new_name += os.path.splitext(files_to_move)[1]

    season = str(int(season))
    new_dir = os.path.join(settings.get_path_opt('dest_dir'), dest_dir, "Season {0}".format(season))

    # make the directory for "Season x" if it doesn't exist
    if not os.path.isdir(new_dir):
        os.makedirs(new_dir)
    new_location = os.path.join(new_dir, new_name)

    # check if the destination file exists or not to prevent overwriting
    if os.path.exists(new_location):
        msg.warn("File {} already exists, skipping".format(new_location))
        return

    # msg.debug("moving " + fileToMove + " to " + destDir)

    # encode the new_location to ascii for printing to terminal
    new_location_unicode = new_location.encode("ascii", "ignore")
    new_name_unicode = new_name.encode("ascii", "ignore")
    old_name_unicode = os.path.basename(files_to_move).encode("ascii", "ignore")
    try:
        shutil.move(files_to_move, new_location)
        msg.debug(old_name_unicode + " => " + new_name_unicode)
        delete_empty_dir(files_to_move)
        with open(settings.get("log_file"), "a") as log:
            log.write("File: {} => {}\n\n".format(files_to_move, new_location_unicode))
    except Exception, e:
        print e
        msg.warn("Failed to move {} to {}".format(files_to_move, new_location_unicode))


def delete_empty_dir(oldFile):
    """
    Deletes the parent directory of the given oldFile if it does not contain
    any more video files
    Args:
        oldFile: the file or directory to delete the parent directory of
    """
    if not settings.get('delete_empty_dirs'):
        return

    current_dir = os.path.dirname(oldFile)
    src_dir = settings.get_path_opt('src_dir')

    # if the parent directory is not the root download directory
    # TODO: delete some of this logging or only print it when in verbose mode
    if os.path.dirname(current_dir) != os.path.dirname(src_dir):
        videos = util.find_by_extension(current_dir, ['avi', 'mp4', 'mkv'])
        if len(videos) == 0:
            print("Deleting containing directory: {}".format(current_dir))
            util.move_to_trash(current_dir)
        else:
            print("Would have deleted {} but video files were found".format(current_dir))
    else:
        print("Not deleting because it's the root download dir")
